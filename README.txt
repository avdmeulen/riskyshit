This is some risky shit to be.

== INSTALL

Make sure you have webpack and/or webpack-dev-server installed globally with:

    npm install -g webpack webpack-dev-server

== RUN

Run with:

    webpack-dev-server --progress --colors

or add some more flavour:

    webpack-dev-server --progress --colors --port 8000 --host 192.168.178.10

== VISIT

With the defaults, visit http://localhost:8080. Don't forget to check the console for more info about the Map structure.

== COMPILE

Just run `webpack` and there should be a bundle.js
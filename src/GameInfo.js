var COLORS = require('./GameConstants.js').COLORS,
    Graphics = PIXI.Graphics,
    Text = PIXI.Text;

function GameInfo(game, settings) {
  Graphics.call(this);

  this.game = game;
  this.settings = settings;

  this.messages = [];

  this.plWidth = this.settings.width / this.game.players.length;

  this.game.players.forEach(function(player, idx) {
    console.log(player, idx);
    this.beginFill(COLORS[player.toUpperCase()]['NORMAL']);
    this.drawRect(idx * this.plWidth, 0, this.plWidth, 19);
    this.endFill();

    var text = new Text(
      player, 
      {font: "16px Futura", fill: "white"}
    );
    text.x = idx * this.plWidth + 10;
    this.addChild(text);
    this.messages[idx] = text;
  }, this);

  this.turnIndicator = new Graphics();
  this.turnIndicator.beginFill(COLORS['ARROW']['NORMAL']);
  this.turnIndicator.drawRect(0, 19, this.plWidth, 5);
  this.turnIndicator.endFill();
  this.addChild(this.turnIndicator);

}

// constructor
GameInfo.prototype = Object.create(Graphics.prototype);
GameInfo.prototype.constructor = GameInfo;
module.exports = GameInfo;

/*
  Update player info with latest game statistics
*/
GameInfo.prototype.updateInfos = function() {
  var stats = this.game.getStatistics();

  this.game.players.forEach(function(player, idx) {
    this.messages[idx].text = stats[player].noTerritories + " | " + stats[player].noArmies;
  }, this);
  this.turnIndicator.x = this.plWidth * this.game.currentPlayerIdx;
}

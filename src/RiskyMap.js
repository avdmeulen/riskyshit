var COLORS = require('./GameConstants.js').COLORS,
    Territory = require('./Territory.js'),
    MapGenerator = require('./MapGenerator.js'),
    Container = PIXI.Container,
    Graphics = PIXI.Graphics,
    Text = PIXI.Text;


var renderMountain = function(riskyMap) {
  var x, y, width, height;
  
  x = riskyMap.settings.width / 2;
  width = riskyMap.settings.width /4;
  y = riskyMap.settings.height / 2;
  height = riskyMap.settings.height / 6;
  
  // var mountain = new PIXI.Ellipse(x, y, width, height);
  var background = new Graphics();
  var blurFilter = new PIXI.filters.BlurFilter();
  blurFilter.blur = 40;

  background.beginFill(0x000000);
  background.drawRect(0, 0, riskyMap.settings.width, riskyMap.settings.height);
  background.endFill();

  var mask = riskyMap.getMask(0x000000, 0xFFFFFF);
  background.addChild(mask);
  
  var mountain = new Graphics();

  mountain.beginFill(0xFFFFFF, 1);
  // riskyMap.lineStyle(4, lineColor);
  mountain.drawEllipse(x, y, width, height);
  mountain.endFill();
  // background.addChild(mountain);
  // background.mask = mask;

  background.filters = [blurFilter];
  // mask.anchor.x = 0.5;
  // mask.anchor.y = 0.5;
  background.scale.x = 0.5;
  background.scale.y = 0.5;
  background.x = background.width / 2;
  background.y = background.height / 2;

  riskyMap.addChild(background);
}

var sample = function(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

var sampleTerritory = function(arr) {
  var territory;
  while ((territory = sample(arr)) == undefined) { ; }
  return territory;
}

var renderMountainLines = function(riskyMap) {
  var startTerritory = sampleTerritory(riskyMap.territories);
  console.log(startTerritory);
  var startEdge = sample(startTerritory.cell.halfedges);
  console.log(startEdge, startEdge.getStartpoint(), startEdge.getEndpoint());

  var line = new Graphics();
  // line.beginFill(0x96472F);
  line.lineStyle(6, 0x000000);
  line.moveTo(startEdge.getStartpoint().x, startEdge.getStartpoint().y);

  var lineCount = 3;
  while (lineCount-- > 0) {
    var endPoint = startEdge.getEndpoint();
    line.lineTo(endPoint.x, endPoint.y);
    
    // get next territory/edge
    var nbSameEdge = startEdge.edge.lSite.voronoiId == startTerritory.cell.site.voronoiId ?
                     startEdge.edge.rSite.voronoiId :
                     startEdge.edge.lSite.voronoiId;
    nbSameEdge = riskyMap.territories[nbSameEdge];

    startEdge = nbSameEdge.cell.halfedges.filter(function(halfedge, idx) {
      return (!startTerritory.cell.sea && halfedge.getStartpoint() == endPoint);
    })[0];
    startTerritory = riskyMap.territories[startEdge.site.voronoiId];

    console.log(startEdge);
  }

  // line.endFill();
  riskyMap.addChild(line);
}

function RiskyMap(settings) {
  Container.call(this);

  // if (typeof settings === 'undefined') {
  //   settings = {};
  // }
  // var defaults = {
  //   width: 800,
  //   height: 600,
  //   noTerritories: 50
  // };
  // for(var i in defaults) { 
  //   if (!settings.hasOwnProperty(i)) {
  //     settings[i] = defaults[i];
  //   }
  // }
  this.settings = settings;

  // The Voronoi diagram defining the map
  this.mapDiagram = MapGenerator.generateMap(
    this.settings.width,
    this.settings.height,
    this.settings.noTerritories
  );
  console.log(this.mapDiagram);

  // The Territory objects that are also Graphics
  // Create first before rendering them, so we can do???
  // Save with index, leaves holes for sea, but faster lookup from voronoiId
  this.territories = [];

  this.mapDiagram.cells.forEach(function(cell, idx) {
    if (!cell.sea) {
      var territory = new Territory(cell);
      this.territories[idx] = territory;
    }
  }, this);
  

  // Render territories and add them to the PIXI.Container
  this.territories.forEach(function(territory) {
    territory.render();
    this.addChild(territory);
  }, this);

  // renderMountain(this);
  // renderMountainLines(this);

  // this.renderTerritoryLines();
}

// constructor
RiskyMap.prototype = Object.create(Container.prototype);
RiskyMap.prototype.constructor = RiskyMap;
module.exports = RiskyMap;

RiskyMap.prototype.setInitialOwners = function(players) {
  var idx = 0;
  this.territories.forEach(function(territory) {
    territory.setOwner(players[idx++ % players.length]);
  });
  console.log(this.territories);
}

// Draw all edges of the mapDiagram as individual lines
RiskyMap.prototype.renderTerritoryLines = function() {
  var edges = this.mapDiagram.edges,
    nEdges = edges.length,
    v;
  if (nEdges) {
    var edge, line;
    while (nEdges--) {
      line = new Graphics();
      line.lineStyle(2, 0x123456, 1);
    
      edge = edges[nEdges];
      v = edge.va;
      line.moveTo(v.x,v.y);
      v = edge.vb;
      line.lineTo(v.x,v.y);

      line.endFill();
      this.addChild(line);
    }  
  }
}

RiskyMap.prototype.getMask = function(bgColor, fgColor) {
  var mask = new Graphics();

  mask.beginFill(bgColor);
  mask.drawRect(0, 0, this.settings.width, this.settings.height);
  mask.endFill();

  mask.beginFill(fgColor);
  mask.lineStyle(2, fgColor, 1);
  
  for (i = 0; i < this.mapDiagram.cells.length; i++) {
    if (!this.mapDiagram.cells[i].sea) {
      mask.drawPolygon(this.mapDiagram.cells[i].getPIXIPolygon());
    }
  }

  mask.endFill();

  return mask;
}

/*
  Create an arrow from x to y and return it.
  Does not add the arrow to a scene/object yet.
*/
RiskyMap.prototype.createArrow = function(x1, y1, x2, y2) {
  var arrow = new Graphics();
  arrow.lineStyle(2, COLORS['ARROW']['NORMAL'], 1);
  arrow.moveTo(x1, y1);
  
  /* 
    Calculate control point of bezier curve (cx, cy).
    Origin is at center (x1,y1), dest in corner (x2,y2).

    D|A
    -+-
    C|B
  */
  var cx, cy;
  if (x2 > x1 && y2 < y1) {
    // A
    cx = x1;
    cy = y2 - 40;
  } else if (x2 > x1) {
    // B
    cx = x2;
    cy = y1 - 40;
  } else { // x2 <= x1
    if (y2 > y1) {
      // C
      cx = x2;
      cy = y1 - 40;
    } else {
      // D
      cx = x1;
      cy = y2 - 40;
    }
  }

  arrow.quadraticCurveTo(cx, cy, x2, y2);
  return arrow;
}

/*
  Render this map.
  This will create new Territories when called, do we want that?..
*/
// RiskyMap.prototype.render = function() {
//   this.territories.forEach(function(territory) {
//     this.removeChild(territory);
//   }, this);
  // this.removeChildren(this.territories);
  
  // Draw territories as polygons
  // for (var i = 0; i < this.territories.length; i++) {
  //   var territory = this.territories[i];
    
  //   // Render and add to scene
  //   territory.render();
  //   this.addChild(territory);
  // }
// }

/*
  All the neighbouring territories of territory.
*/
RiskyMap.prototype.getNeighbourTerritories = function(territory) {
  return territory.cell.neighbourIds.map(function(idx) {
    return this.territories[idx];
  }, this).filter(function(el){
    return el !== undefined;
  });
}

/*
  Does the territory have at least one friendly neighbour
*/
RiskyMap.prototype.hasFriendlyNeighbour = function(territory) {
  return this.getNeighbourTerritories(territory).some(function(neighbour) {
    return neighbour.owner == territory.owner;
  });
}

/*
  Does the territory have at least one enemy neighbour
*/
RiskyMap.prototype.hasEnemyNeighbour = function(territory) {
  return this.getNeighbourTerritories(territory).some(function(neighbour) {
    return neighbour.owner != territory.owner;
  });
}

/*
  All the territories on the map belonging to owner
*/
RiskyMap.prototype.getPlayerTerritories = function(owner) {
  return this.territories.filter(function(territory) {
    return territory.owner == owner;
  });
}

/*
  Return the territories where owner can attack from.
  Must contain at least 1 army and must neighbour an enemy.
*/
RiskyMap.prototype.getAttackTerritories = function(owner) {
  return this.territories.filter(function(territory) {
    if (territory.owner == owner && territory.armies > 1) {
      // is there a neighbouring enemy?
      return this.getNeighbourTerritories(territory).some(function(neighbour) {
        return neighbour.owner != owner;
      });
    } else {
      // cannot attack from this territory
      return false;
    }
  }, this);
}

/*
  Return the territories where owner can move armies from.
  Must contain at least 1 army and must neighbour a friend.
*/
RiskyMap.prototype.getSourceTerritories = function(owner) {
  return this.territories.filter(function(territory) {
    if (territory.owner == owner && territory.armies > 1) {
      // is there a neighbouring friend?
      return this.getNeighbourTerritories(territory).some(function(neighbour) {
        return neighbour.owner == owner;
      });
    } else {
      // cannot source from this territory
      return false;
    }
  }, this);
}



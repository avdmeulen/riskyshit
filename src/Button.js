var Graphics = PIXI.Graphics,
    Text = PIXI.Text;

/*
  An interactive button with a border and text
*/
function Button(width, height, text) {
  Graphics.call(this);

  this.beginFill(0xFFFFFF);
  this.lineStyle(4, 0x6B6B6B, 1);
  this.drawRect(0, 0, width, height);
  this.endFill();
  
  var fontSize = height - 6;
  this.msg = new Text(
    text, 
    {font: fontSize + "px Futura", fill: "black"}
  );
  this.msg.anchor.x = 0.5;
  this.msg.anchor.y = 0.5;
  this.msg.x = width / 2;
  this.msg.y = height / 2;
  this.addChild(this.msg);

  this.interactive = true;
  this.buttonMode = true;
}

// constructor
Button.prototype = Object.create(Graphics.prototype);
Button.prototype.constructor = Button;
module.exports = Button;

Button.prototype.setText = function(text) {
  this.msg.text = text;
}
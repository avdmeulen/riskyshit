var MapGenerator = require('./MapGenerator.js'),
    Territory = require('./Territory.js'),
    COLORS = require('./GameConstants.js').COLORS,
    Game = require('./Game.js'),
    Turn = require('./Turn.js');

//Aliases
var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Texture = PIXI.Texture,
    Sprite = PIXI.Sprite,
    Text = PIXI.Text,
    Graphics = PIXI.Graphics,
    Game = require('./Game.js'),
    RiskyMap = require('./RiskyMap.js'),
    GameInfo = require('./GameInfo.js');

// Some settings
var width = 800, 
    height = 600,
    noTerritories = 50;

//Create a Pixi stage and renderer and add the 
//renderer.view to the DOM
var stage = new Container(),
    renderer = autoDetectRenderer(width, height, { antialias: true });
document.body.appendChild(renderer.view);

loader
  .add("images/dice.json")
  .load(setup);

//Define variables that might be used in more 
//than one function
var state, polygons, message, gameScene, gameOverScene,
  selectedTerritory, selectedNeighbours, cat, arrows, 
  map, game, turn, gameInfo,
  frameCounter = 0;

function updateInfos() {
  gameInfo.updateInfos();
}

function setup() {
  // cat = new Sprite(resources["images/cat.png"].texture);
  // arrows = [];

  //Make the game scene and add it to the stage
  gameScene = new Container();
  stage.addChild(gameScene);

  map = new RiskyMap({
    width: width,
    height: height,
    noTerritories: 50
  });
  console.log(map);
  gameScene.addChild(map);
  
  // WOW!
  // gameScene.addChild(map.getMask(0x8bc5ff, 0x000000));

  game = new Game(map);
  game.startTurn();


  gameInfo = new GameInfo(game, { width: width });
  gameScene.addChild(gameInfo);

  
  /* Game over */

  //Create the `gameOver` scene
  gameOverScene = new Container();
  stage.addChild(gameOverScene);

  //Make the `gameOver` scene invisible when the game first starts
  gameOverScene.visible = false;

  //Create the text sprite and add it to the `gameOver` scene
  message = new Text(
    "The End!", 
    {font: "64px Futura", fill: "white"}
  );
  message.x = 120;
  message.y = stage.height / 2;// - 32;
  gameOverScene.addChild(message);

  //Set the game state
  state = play;

  //Start the game loop
  gameLoop();
}

function gameLoop() {
  //Loop this function 60 times per second
  requestAnimationFrame(gameLoop);

  //Update the current game state
  state();

  //Render the stage
  renderer.render(stage);
}

function play() {
  // Update info every 10 frames. Bit hacky?
  if ((++frameCounter % 10) == 0) {
    updateInfos();
    frameCounter = 0;
  }


}

function end() {
  gameScene.visible = false;
  gameOverScene.visible = true;
}


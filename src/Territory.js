var COLORS = require('./GameConstants.js').COLORS;

var Graphics = PIXI.Graphics,
    Text = PIXI.Text;

// The code executed when a Territory is rendered.
var states = {
  normal: function() {
    var bgColor = this.owner ? 
        COLORS[this.owner.toUpperCase()]['NORMAL'] :
        COLORS['BLACK'];
    this.renderPolygon(bgColor, COLORS['WHITE']);
  },
  highlighted: function() {
    var bgColor = this.owner ? 
        COLORS[this.owner.toUpperCase()]['LIGHT'] :
        COLORS['BLACK'];
    this.renderPolygon(bgColor, COLORS['WHITE']);
    // this.message.text = this.idx;
  },
  selected: function() {
    var bgColor = this.owner ? 
        COLORS[this.owner.toUpperCase()]['DARK'] :
        COLORS['BLACK'];
    var lineColor = COLORS['ARROW']['NORMAL'];
    this.renderPolygon(bgColor, COLORS['WHITE']);
  }
}

/**
  A territory with a polygon shape and other properties.
  Extend PIXI.Graphics and store extra information about a territory.
*/
function Territory(cell) {
  Graphics.call(this);

  this.cell = cell;
  this.idx = cell.site.voronoiId;
  this.name = 'Territory ' + cell.site.voronoiId;

  this.owner = false;
  this.startArmies = parseInt(Math.max(1, (cell.getArea() / 1000)));
  this.armies = this.startArmies;
  
  // Make the territory interactive
  this.hitArea = cell.getPIXIPolygon();
  this.interactive = true;

  // Setup text label
  this.message = new Text(
    this.armies,
    //this.idx,
    {font: "16px sans-serif", fill: "white"}
  );
  this.message.anchor.set(0.5, 0.5);
  this.message.position.set(cell.getCenter().x, cell.getCenter().y);
  this.addChild(this.message);

  // Setup the render states
  this.states = states;
  this.state = 'normal';
}

// constructor
Territory.prototype = Object.create(Graphics.prototype);
Territory.prototype.constructor = Territory;
module.exports = Territory;

/**
  Render the territory with the given style.
*/
Territory.prototype.renderPolygon = function(fillColor, lineColor) {
  this.beginFill(fillColor, 1); //0.5);
  this.lineStyle(2, lineColor);
  this.drawPolygon(this.hitArea);
  this.endFill();

  // the dots fed to the voronoi generator
  // this.beginFill(0xFFFFFF);
  // this.drawCircle(this.cell.site.x, this.cell.site.y, 2);
  // this.endFill();
}

Territory.prototype.render = function() {
  // Call the current state function to render shit
  this.states[this.state].apply(this);

  return this;
}

Territory.prototype.addArmies = function(noArmies) {
  this.armies += noArmies;
  this.message.text = this.armies;

  return this;
}

Territory.prototype.removeArmies = function(noArmies) {
  this.armies -= noArmies;
  this.message.text = this.armies;

  return this;
}

Territory.prototype.setState = function(newState) {
  this.state = newState;
  this.render();

  return this;
}

Territory.prototype.setOwner = function(newOwner) {
  this.owner = newOwner;
  this.render();

  return this;
}

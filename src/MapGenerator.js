var Utils = require('./Utils.js'),
    randomInt = Utils.randomInt,
    Graphics = PIXI.Graphics,
    Polygon = PIXI.Polygon;

// Use Voronoi diagram to generate territories/cells
// See https://github.com/gorhill/Javascript-Voronoi
// Let's add some extra functions:

Voronoi.prototype.Cell.prototype.getPolygonPoints = function() {
  var halfedges = this.halfedges,
      nHalfedges = halfedges.length,
      v = halfedges[0].getStartpoint(),
      points = [];
  
  points.push(v.x, v.y);
  for (var iHalfedge = 0; iHalfedge < nHalfedges; iHalfedge++) {
    v = halfedges[iHalfedge].getEndpoint();
    points.push(v.x, v.y);
  }
  
  return points;
}

// Useful with PIXI.Graphics.drawPolygon
Voronoi.prototype.Cell.prototype.getPIXIPolygon = function() {
  return new Polygon(this.getPolygonPoints());
}

// The center determined by the bounding box of the polygon.
Voronoi.prototype.Cell.prototype.getCenter = function() {
  var bBox = this.getBbox();
  return {
    x: bBox.x + (bBox.width / 2),
    y: bBox.y + (bBox.height / 2)
  };
}

// The area of the polygon.
Voronoi.prototype.Cell.prototype.getArea = function() {
  // See http://www.mathopenref.com/coordpolygonarea.html
  var xs = [],
      ys = [],
      noXs,
      point,
      area = 0,
      i, nexti;

  // build arrays of x and y coordinates
  for (i = 0; i < this.halfedges.length; i++) {
    point = this.halfedges[i].getStartpoint();
    xs.push(point.x);
    ys.push(point.y);
  };

  // get area using formula (see webpage)
  noXs = xs.length;
  for (i = 0; i < noXs; i++) {
    nexti = (i + 1) % noXs;
    area += xs[i] * ys[nexti] - ys[i] * xs[nexti];
  }
  return Math.abs(area / 2).toFixed(0);
}

// Voronoi.prototype.Diagram.prototype.getMountain = function() {
  
// }

/*
Voronoi.prototype.getNormalizedAreas = function() {
  var areas = this.cells.map(function(cell) {
    return cell.getArea();
  });

}
*/

/* Make the borders look more 'natural' */
var addBorderNoise = function(diagram) {
  // TODO!
  return diagram;
}

var MapGenerator = {

  generateMap: function(width, height, no_territories) {
    var voronoi = new Voronoi();
    var borderOffset = 20;
    var bbox = {
      xl: borderOffset, 
      xr: width - borderOffset, 
      yt: borderOffset, 
      yb: height - borderOffset
    }; // xl is x-left, xr is x-right, yt is y-top, and yb is y-bottom
    // var sites = [ {x: 200, y: 200}, {x: 50, y: 250}, {x: 400, y: 100} /* , ... */ ];
    var sites = [];
    var i, j;

    for (i = 0; i < no_territories; i++) {
      var territory = {};
      territory.x = randomInt(3 * borderOffset, width - (3 * borderOffset));
      territory.y = randomInt(3 * borderOffset, height - (3 * borderOffset));
      sites.push(territory);
    }

    // a 'vertex' is an object exhibiting 'x' and 'y' properties. The
    // Voronoi object will add a unique 'voronoiId' property to all
    // sites. The 'voronoiId' can be used as a key to lookup the associated cell
    // in diagram.cells.

    var diagram = voronoi.compute(sites, bbox);

    // Mark cells as 'sea' if next to screen border
    var cell, edge, startpoint, i, j;
    for (i = 0; i < diagram.cells.length; i++) {
      cell = diagram.cells[i];
      cell.sea = false;
      for (j = 0; j < cell.halfedges.length; j++) {
        edge = cell.halfedges[j].edge;
        startpoint = cell.halfedges[j].getStartpoint();
        // compare with some extra offset for more sea
        if (startpoint.x <= borderOffset + 5 || 
            startpoint.x >= width - borderOffset - 5 ||
            startpoint.y <= borderOffset + 5 ||
            startpoint.y >= height - borderOffset - 5) {
          cell.sea = true;
        }
      };
    };

    // TODO: define more interesting properties for each cell:
    // size, distance_to_border, color, best place to put text, etc

    // Determine neighbours
    for (i = 0; i < diagram.cells.length; i++) {
      cell = diagram.cells[i];
      cell.neighbourIds = [];
      for (j = 0; j < cell.halfedges.length; j++) {
        edge = cell.halfedges[j].edge;
        if (edge.lSite && edge.lSite.voronoiId !== i && cell.neighbourIds.indexOf(edge.lSite.voronoiId) === -1) {
          cell.neighbourIds.push(edge.lSite.voronoiId);
        }
        if (edge.rSite && edge.rSite.voronoiId !== i && cell.neighbourIds.indexOf(edge.rSite.voronoiId) === -1) {
          cell.neighbourIds.push(edge.rSite.voronoiId);
        }
      }
    }

    // Remove islands (cells surrounded by only sea)
    var allSea, neighbour;
    for (i = 0; i < diagram.cells.length; i++) {
      cell = diagram.cells[i];
      if (cell.sea) {
        continue;
      }
      allSea = true;
      for (j = 0; j < cell.neighbourIds.length; j++) {
        neighbour = diagram.cells[cell.neighbourIds[j]];
        if (!neighbour.sea) {
          allSea = false;
          break;
        }
      }
      cell.sea = allSea;
    }

    // Make the borders more 'zig zag'
    diagram = addBorderNoise(diagram);

    return diagram;
  }

};

module.exports = MapGenerator;
var GameConstants = {
  COLORS: {
    BLACK: 0x000000,
    WHITE: 0xFFFFFF,
    RED: {
      DARK: 0x800000,
      NORMAL: 0xB20000,
      LIGHT: 0xFF0000
    },
    BLUE: {
      DARK: 0x000080,
      NORMAL: 0x0000FF,
      LIGHT: 0x004EFF
    },
    GREEN: {
      DARK: 0x008000,
      NORMAL: 0x00FF00,
      LIGHT: 0x00FF4E
    },
    ARROW: {
      NORMAL: 0xFFD800
    }
  }
};

module.exports = GameConstants;

// var STATES = ['placeArmy', 'selectAttack', 'selectDefense', 'battleResult', 'selectSource', 'selectDest', 'endTurn']

function Turn(game, player) {
  this.game = game;
  this.player = player;

  // TODO: make nrs dependent on game props
  this.placeArmiesLeft = Math.max(3, Math.floor(this.game.getStatistics()[this.player].noTerritories / 3));
  this.attacksLeft = 5;
  this.movesArmiesLeft = 3;

  // used during attack phase
  this.attackTerritory = null;
  this.defenseTerritory = null;

  // used during transfer phase
  this.sourceTerritory = null;
  this.destTerritory = null;

  // initial state
  this.state = this.placeArmy;
}

module.exports = Turn;

/*
  Place an army on one of the players territories
*/
Turn.prototype.placeArmy = function() {
  var playerTerritories = this.game.map.getPlayerTerritories(this.player);
  // console.log('Turn.placeArmy()', this);

  var handleTerritoryMouseOut = function(ev) {
    ev.target.setState('normal');
  }

  var handleTerritoryMouseOver = function(ev) {
    ev.target.setState('highlighted');
  }

  var handleTerritoryClick = function(ev) {
    var territory = ev.target;
    // console.log('clicked on: ', territory.name, territory);

    territory.addArmies(1);
    this.placeArmiesLeft -= 1;

    if (this.placeArmiesLeft == 0) {
      console.log('no more armies to place');
      playerTerritories.forEach(function(territory) {
        territory.removeListener('touchend', handleTerritoryClick)
        territory.removeListener('click', handleTerritoryClick);
        territory.removeListener('mouseover', handleTerritoryMouseOver);
        territory.removeListener('mouseout', handleTerritoryMouseOut);
      });

      // cannot attack from here, so dehighlight
      if (!this.game.map.hasEnemyNeighbour(territory)) {
        territory.setState('normal');
      }

      this.state = this.selectAttack;
      this.state();
    }
  }

  // add listeners to player territories
  playerTerritories.forEach(function(territory) {
    territory.on('mouseout', handleTerritoryMouseOut, this);
    territory.on('mouseover', handleTerritoryMouseOver, this);
    territory.on('click', handleTerritoryClick, this);
    territory.on('touchend', handleTerritoryClick, this);
  }, this);
}

/*
  Select an own territory to attack from
*/
Turn.prototype.selectAttack = function() {
  this.attackTerritory = null;
  this.defenseTerritory = null;
  // console.log('Turn.selectAttack()', this);

  var handleTerritoryMouseOut = function(ev) {
    ev.target.setState('normal');
  }

  var handleTerritoryMouseOver = function(ev) {
    ev.target.setState('highlighted');
  }

  var handleTerritoryClick = function(ev) {
    var territory = ev.target;
    // console.log('clicked on: ', territory.name, territory);

    this.attackTerritory = territory;

    var playerTerritories = this.game.map.getPlayerTerritories(this.player);
    playerTerritories.forEach(function(territory) {
      territory.removeListener('touchend', handleTerritoryClick);
      territory.removeListener('click', handleTerritoryClick);
      territory.removeListener('mouseover', handleTerritoryMouseOver);
      territory.removeListener('mouseout', handleTerritoryMouseOut);
    });
    territory.setState('normal');

    this.game.btnNextTurn.removeListener('touchend', handleEndAttackClick);
    this.game.btnNextTurn.removeListener('click', handleEndAttackClick);
    this.game.btnNextTurn.visible = false;

    this.state = this.selectDefense;
    this.state();
  }

  var handleEndAttackClick = function(ev) {
    console.log('end attack click');

    var playerTerritories = this.game.map.getPlayerTerritories(this.player);
    playerTerritories.forEach(function(territory) {
      territory.removeListener('touchend', handleTerritoryClick);
      territory.removeListener('click', handleTerritoryClick);
      territory.removeListener('mouseover', handleTerritoryMouseOver);
      territory.removeListener('mouseout', handleTerritoryMouseOut);
    });

    this.game.btnNextTurn.removeListener('touchend', handleEndAttackClick);
    this.game.btnNextTurn.removeListener('click', handleEndAttackClick);
    this.game.btnNextTurn.visible = false;

    this.state = this.selectSource;
    this.state();
  }

  // add click listeners to player attack territories
  var playerTerritories = this.game.map.getAttackTerritories(this.player);
  playerTerritories.forEach(function(territory) {
    territory.on('mouseout', handleTerritoryMouseOut, this);
    territory.on('mouseover', handleTerritoryMouseOver, this);
    territory.on('click', handleTerritoryClick, this);
    territory.on('touchend', handleTerritoryClick, this);
  }, this);

  // show 'end attack' button
  this.game.btnNextTurn.setText('End attack');
  this.game.btnNextTurn.on('click', handleEndAttackClick, this);
  this.game.btnNextTurn.on('touchend', handleEndAttackClick, this);
  this.game.btnNextTurn.visible = true;
}

/*
  Select a neighbour enemy territory to attack
*/
Turn.prototype.selectDefense = function() {
  var arrows = [];
  // console.log('Turn.selectDefense()', this);

  var handleTerritoryMouseOut = function(ev) {
    ev.target.setState('selected');
    this.game.map.removeChild(arrows.shift());
  }

  var handleTerritoryMouseOver = function(ev) {
    var neighbour = ev.target;
    neighbour.setState('highlighted');
    
    var selCenter = this.attackTerritory.cell.getCenter();
    var nbCenter = neighbour.cell.getCenter();
    
    var arrow = this.game.map.createArrow(selCenter.x, selCenter.y, nbCenter.x, nbCenter.y);
    this.game.map.addChild(arrow);
    arrows.push(arrow);
  }

  var handleTerritoryClick = function(ev) {
    var territory = ev.target;
    // console.log('clicked on: ', territory.name, territory);

    this.defenseTerritory = territory;
    this.game.map.removeChild(arrows.shift());

    var neighbourTerritories = this.game.map.getNeighbourTerritories(this.attackTerritory);
    neighbourTerritories.forEach(function(territory) {
      if (territory.owner != this.attackTerritory.owner) {
        territory.removeListener('touchend', handleTerritoryClick);
        territory.removeListener('click', handleTerritoryClick);
        territory.removeListener('mouseover', handleTerritoryMouseOver);
        territory.removeListener('mouseout', handleTerritoryMouseOut);
        territory.setState('normal');
      }
    }, this);
    this.attackTerritory.removeListener('touchend', handleTerritoryClick);
    this.attackTerritory.removeListener('click', handleTerritoryClick);

    if (territory === this.attackTerritory) {
      // go back to attack state
      territory.setState('highlighted');
      this.state = this.selectAttack;
      this.state();
    } else {
      territory.setState('normal'); // unhighlight current territory
      this.state = this.battleResult;
      this.state();
    }
  }

  // add listener to player territory for cancel
  this.attackTerritory.on('click', handleTerritoryClick, this);
  this.attackTerritory.on('touchend', handleTerritoryClick, this);

  // add listeners to enemy defense territories
  var neighbourTerritories = this.game.map.getNeighbourTerritories(this.attackTerritory);
  neighbourTerritories.forEach(function(territory) {
    if (territory.owner != this.attackTerritory.owner) {
      territory.on('mouseout', handleTerritoryMouseOut, this);
      territory.on('mouseover', handleTerritoryMouseOver, this);
      territory.on('click', handleTerritoryClick, this);
      territory.on('touchend', handleTerritoryClick, this);
      territory.setState('selected');
    }
  }, this);
}

/*
  Come up with a battle result and show it 
*/
Turn.prototype.battleResult = function() {
  var defenseTerritoryOwner = this.defenseTerritory.owner;

  console.log('battle between ' + 
    this.attackTerritory.name + ' (' + this.attackTerritory.armies + ') and ' + 
    this.defenseTerritory.name + ' (' + this.defenseTerritory.armies + ')');

  // Simulate battle
  var result = this.game.simulateBattle(this.attackTerritory, this.defenseTerritory);
  // console.log(result);

  var diceResult = {};
  diceResult[this.attackTerritory.owner] = result[0];
  diceResult[defenseTerritoryOwner] = result[1];
  this.game.showDiceResults(diceResult);

  // Game over?
  var stats = this.game.getStatistics();
  for (var player in this.game.players) {
    if (stats[this.game.players[player]].noTerritories == 0) {
      // WHAT NOW?
      console.log('GAME OVER!');
    }
  }
  // stats.forEach(function(player) {
  //   if (stats[player].noTerritories == 0) {
  //     // WHAT NOW?
  //     console.log('GAME OVER!');
  //   }
  // })

  // Start another attack
  this.attackTerritory = null;
  this.defenseTerritory = null;
  this.state = this.selectAttack;
  this.state();
}

/*
  Select a country to transfer armies from.
*/
Turn.prototype.selectSource = function() {
  this.sourceTerritory = null;
  this.destTerritory = null;
  // console.log('Turn.selectAttack()', this);

  var handleTerritoryMouseOut = function(ev) {
    ev.target.setState('normal');
  }

  var handleTerritoryMouseOver = function(ev) {
    ev.target.setState('highlighted');
  }

  var handleTerritoryClick = function(ev) {
    var territory = ev.target;
    console.log('clicked on source: ', territory.name, territory);

    this.sourceTerritory = territory;

    var playerTerritories = this.game.map.getSourceTerritories(this.player);
    playerTerritories.forEach(function(territory) {
      territory.removeListener('touchend', handleTerritoryClick);
      territory.removeListener('click', handleTerritoryClick);
      territory.removeListener('mouseover', handleTerritoryMouseOver);
      territory.removeListener('mouseout', handleTerritoryMouseOut);
    });
    territory.setState('normal');

    this.game.btnNextTurn.removeListener('click', handleEndTurnClick);
    this.game.btnNextTurn.removeListener('touchend', handleEndTurnClick);
    this.game.btnNextTurn.visible = false;

    this.state = this.selectDest;
    this.state();
  }

  var handleEndTurnClick = function(ev) {
    console.log('end turn click');

    var playerTerritories = this.game.map.getSourceTerritories(this.player);
    playerTerritories.forEach(function(territory) {
      territory.removeListener('touchend', handleTerritoryClick);
      territory.removeListener('click', handleTerritoryClick);
      territory.removeListener('mouseover', handleTerritoryMouseOver);
      territory.removeListener('mouseout', handleTerritoryMouseOut);
    });

    this.game.btnNextTurn.removeListener('touchend', handleEndTurnClick);
    this.game.btnNextTurn.removeListener('click', handleEndTurnClick);
    this.game.btnNextTurn.visible = false;

    // WHAT NOW??!!
    this.game.startTurn();
  }

  // add click listeners to player attack territories
  var playerTerritories = this.game.map.getSourceTerritories(this.player);
  playerTerritories.forEach(function(territory) {
    if (territory.armies > 1) {
      territory.on('mouseout', handleTerritoryMouseOut, this);
      territory.on('mouseover', handleTerritoryMouseOver, this);
      territory.on('click', handleTerritoryClick, this);
      territory.on('touchend', handleTerritoryClick, this);
    }
  }, this);

  // show 'end attack' button
  this.game.btnNextTurn.setText('End turn');
  this.game.btnNextTurn.on('click', handleEndTurnClick, this);
  this.game.btnNextTurn.on('touchend', handleEndTurnClick, this);
  this.game.btnNextTurn.visible = true;
}


/*
  Select a neighbour enemy territory to attack
*/
Turn.prototype.selectDest = function() {
  // console.log('Turn.selectDefense()', this);

  var handleTerritoryMouseOut = function(ev) {
    ev.target.setState('selected');
  }

  var handleTerritoryMouseOver = function(ev) {
    ev.target.setState('highlighted');
  }

  var handleTerritoryClick = function(ev) {
    var territory = ev.target;
    console.log('clicked on dest: ', territory.name, territory);

    this.destTerritory = territory;
    
    var neighbourTerritories = this.game.map.getNeighbourTerritories(this.sourceTerritory);
    neighbourTerritories.forEach(function(territory) {
      if (territory.owner == this.sourceTerritory.owner) {
        territory.removeListener('touchend', handleTerritoryClick);
        territory.removeListener('click', handleTerritoryClick);
        territory.removeListener('mouseover', handleTerritoryMouseOver);
        territory.removeListener('mouseout', handleTerritoryMouseOut);
        territory.setState('normal');
      }
    }, this);
    this.sourceTerritory.removeListener('touchend', handleTerritoryClick);
    this.sourceTerritory.removeListener('click', handleTerritoryClick);

    if (territory === this.sourceTerritory) {
      // go back to select source state
      territory.setState('highlighted');
      this.state = this.selectSource;
      this.state();
    } else {
      // clicked on a neighbour
      this.destTerritory.addArmies(1);
      this.sourceTerritory.removeArmies(1);

      territory.setState('normal'); // unhighlight current territory
      this.state = this.selectSource;
      this.state();
    }
  }

  // add listener to player territory for cancel
  this.sourceTerritory.on('click', handleTerritoryClick, this);
  this.sourceTerritory.on('touchend', handleTerritoryClick, this);

  // add listeners to friendly neighbours territories
  var neighbourTerritories = this.game.map.getNeighbourTerritories(this.sourceTerritory);
  neighbourTerritories.forEach(function(territory) {
    if (territory.owner == this.sourceTerritory.owner) {
      territory.on('mouseout', handleTerritoryMouseOut, this);
      territory.on('mouseover', handleTerritoryMouseOver, this);
      territory.on('click', handleTerritoryClick, this);
      territory.on('touchend', handleTerritoryClick, this);
      territory.setState('selected');
    }
  }, this);
}

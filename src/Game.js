var randomInt = require('./Utils.js').randomInt,
    Turn = require('./Turn.js'),
    Button = require('./Button'),
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite;

function Game(map) {
  this.players = ['red', 'blue', 'green'];
  this.map = map;
  this.map.setInitialOwners(this.players);
  
  this.currentPlayerIdx = randomInt(0, this.players.length);
  this.currentPlayer = this.players[this.currentPlayerIdx];

  this.turn = null;

  this.diceSprites = [];
  this.diceTimerId;

  this.btnNextTurn = new Button(160, 30, "Next turn");
  this.btnNextTurn.x = this.map.settings.width - 170;
  this.btnNextTurn.y = this.map.settings.height - 40;
  this.map.addChild(this.btnNextTurn);
  this.btnNextTurn.visible = false;
}

Game.prototype.getPlayer = function() {
  return this.players[this.currentPlayerIdx];
}

Game.prototype.getEnemy = function() {
  return this.players[(this.currentPlayerIdx + 1) % this.players.length];
}

Game.prototype.startTurn = function() {
  // remove dead players from game
  var stats = this.getStatistics();
  this.players.forEach(function(player, idx) {
    if (stats[player].noTerritories == 0) {
      delete this.players[idx];
    }
  }, this);

  this.turn = this.getNextTurn();
  return this.turn;
}

Game.prototype.endGame = function() {
  console.log("GAME OVER!", this.getStatistics().currentPlayer, "has won.");
}

Game.prototype.getNextTurn = function() {
  this.currentPlayerIdx = (this.currentPlayerIdx + 1) % this.players.length;
  this.currentPlayer = this.players[this.currentPlayerIdx];

  this.turn = new Turn(this, this.getPlayer());
  this.turn.state();

  return this.turn;
}

/*
  Get player statistics
*/
Game.prototype.getStatistics = function() {
  var stats = {
    currentPlayer: this.currentPlayer
  };
  // object with everything set to zero
  this.players.forEach(function(player) {
    stats[player] = {
      noTerritories: 0,
      noArmies: 0
    }
  });
  this.map.territories.forEach(function(territory) {
    stats[territory.owner].noTerritories += 1;
    stats[territory.owner].noArmies += territory.armies;
  });
  return stats;
}

Game.prototype.getDie = function(color, number) {
  return new Sprite(
    resources["images/dice.json"].textures[color + "_" + number + ".jpg"]
  );
}


/*
  Show dice on screen.
*/
Game.prototype.showDiceResults = function(dice) {
  clearTimeout(this.diceTimerId);
  this.diceTimerId = null;
  this.removeDiceResults();

  var rowCount = 0;
  for (var color in dice) {
    for (var i = 0; i < dice[color].length; i++) {
      var die = this.getDie(color, dice[color][i]);
      die.x = i * die.width + 25;
      die.y = rowCount * die.height + 25;
      this.map.addChild(die);
      this.diceSprites.push(die);
    }
    rowCount++;
  }
  this.diceTimerId = setTimeout(this.removeDiceResults.bind(this), 3000);
}

Game.prototype.removeDiceResults = function() {
  this.diceTimerId = null;
  for (var i = this.diceSprites.length - 1; i >= 0; i--) {
    this.map.removeChild(this.diceSprites[i]);
    delete this.diceSprites[i];
  };
  this.diceSprites = [];
}

/*
  Simulate a classic Risk battle.
  Will add/remove armies and conquer territory as needed.
  Returns the dice thrown.
*/
Game.prototype.simulateBattle = function(attackTerritory, defenseTerritory) {
  var noADice = Math.min(3, attackTerritory.armies - 1),
      noDDice = Math.min(2, defenseTerritory.armies),
      aDice = [], 
      dDice = [],
      i,
      territoryTaken = false,
      armiesTransferred,
      maxNoCompare;

  for (i = 0; i < noADice; i++) {
    aDice.push(randomInt(1, 6));
  };
  for (i = 0; i < noDDice; i++) {
    dDice.push(randomInt(1, 6));
  };
  aDice = aDice.sort().reverse();
  dDice = dDice.sort().reverse();
  console.log("attack:", aDice, "defense:", dDice);

  maxNoCompare = Math.min(aDice.length, dDice.length)

  for (i = 0; i < maxNoCompare; i++) {
    if (aDice[i] > dDice[i]) {
      // attacker wins with this die
      defenseTerritory.removeArmies(1);

      if (defenseTerritory.armies == 0) {
        // attacker takes country, transfer half of the armies
        defenseTerritory.owner = attackTerritory.owner;
        armiesTransferred = Math.floor(attackTerritory.armies / 2);
        attackTerritory.removeArmies(armiesTransferred);
        defenseTerritory.addArmies(armiesTransferred);
        defenseTerritory.render();
        console.log('Territory conquered by ' + attackTerritory.owner + '! Transfer ' + armiesTransferred + ' armies.')
        return [aDice, dDice];
      }
    } else {
      // defender wins with this die
      attackTerritory.removeArmies(1);
    }
  };

  return [aDice, dDice];
}

module.exports = Game;
